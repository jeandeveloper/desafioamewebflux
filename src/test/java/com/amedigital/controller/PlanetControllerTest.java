package com.amedigital.controller;

import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.amedigital.entity.Planet;
import com.amedigital.repository.PlanetRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AutoConfigureWebTestClient
@SpringBootTest
public class PlanetControllerTest {
	
	@MockBean
	PlanetRepository planetRepository;
	
	@Autowired
	private WebTestClient webClient;
	
	@Test
	void criarPlaneta() {
		Planet planet = new Planet();
		planet.setName("Novo planeta");
		planet.setTerrain("Terrain");
		planet.setClimate("Climate");
		
		Mockito.when(planetRepository.save(Mockito.any())).thenReturn(Mono.just(planet));
		
		webClient.post()
			.uri("/planet")
			.contentType(MediaType.APPLICATION_JSON)
			.body(BodyInserters.fromPublisher(Mono.just(planet), Planet.class))
			.exchange()
			.expectStatus().isOk();
		
		Mockito.verify(planetRepository, times(1)).save(Mockito.any());
	}
	
	@Test
    void testFindByName() 
    {
		Planet planet = new Planet();
		planet.setName("Novo planeta");
		planet.setTerrain("Terrain");
		planet.setClimate("Climate");
         
        List<Planet> list = new ArrayList<Planet>();
        list.add(planet);
         
        Flux<Planet> planetFlux = Flux.fromIterable(list);
         
        Mockito
            .when(planetRepository.findByNameContainingIgnoreCase(Mockito.anyString()))
            .thenReturn(planetFlux);
 
        webClient.get().uri("/planet/name/{name}", "Test")
            .header(HttpHeaders.ACCEPT, "application/json")
            .exchange()
            .expectStatus().isOk()
            .expectBodyList(Planet.class);
         
        Mockito.verify(planetRepository, times(1)).findByNameContainingIgnoreCase(Mockito.anyString());
    }
	
	 @Test
	    void testFindById() 
	    {
			Planet planet = new Planet();
			planet.setName("Novo planeta");
			planet.setTerrain("Terrain");
			planet.setClimate("Climate");
	             
	        Mockito
	            .when(planetRepository.findById(Mockito.anyString()))
	            .thenReturn(Mono.just(planet));
	 
	        webClient.get().uri("/planet/{id}", 100)
	            .exchange()
	            .expectStatus().isOk()
	            .expectBody()
	            .jsonPath("$.name").isNotEmpty()
	            .jsonPath("$.name").isEqualTo("Novo planeta")
	            .jsonPath("$.climate").isEqualTo("Climate");
	         
	        Mockito.verify(planetRepository, times(1)).findById(Mockito.anyString());
	    }
	 
	    @Test
	    void testDeletePlanet() 
	    {
	        Mono<Void> voidReturn  = Mono.empty();
	        Mockito
	            .when(planetRepository.deleteById(Mockito.anyString()))
	            .thenReturn(voidReturn);
	 
	        webClient.delete().uri("/planet/{id}", "asdf")
	            .exchange()
	            .expectStatus().isNoContent();
	    }

}
