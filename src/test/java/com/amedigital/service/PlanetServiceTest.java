package com.amedigital.service;

import javax.validation.constraints.AssertTrue;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.amedigital.entity.Planet;
import com.amedigital.repository.PlanetRepository;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
public class PlanetServiceTest {

	@MockBean
	PlanetRepository planetRepository;

	@Autowired 
	PlanetService planetService;

	@Test
	void criarPlaneta() {
		Planet planet = new Planet();
		planet.setName("Novo planeta");
		planet.setTerrain("Terrain");
		planet.setClimate("Climate");

		Mockito.when(planetRepository.save(Mockito.any())).thenReturn(Mono.just(planet));
		Mono<Planet> mono = planetService.save(planet);

		StepVerifier
		.create(mono)
		.consumeNextWith(newPlanet -> {
			assertEquals(newPlanet.getName(), planet.getName());
		})
		.verifyComplete();
	}

	@Test
	public void findPlanetByName() {
		Planet planet = new Planet();
		planet.setName("Novo planeta");
		planet.setTerrain("Terrain");
		planet.setClimate("Climate");

		when(planetRepository.findByNameContainingIgnoreCase(Mockito.anyString())).thenReturn(Mono.just(planet).flux());
		Flux<Planet> planetFlux = planetService.findByName("Novo planeta");
		
		StepVerifier
		.create(planetFlux)
		.consumeNextWith(newPlanet -> {
			assertEquals(newPlanet.getName(), planet.getName());
		})
		.verifyComplete();
	}

}
