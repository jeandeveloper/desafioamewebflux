package com.amedigital.webclient;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.amedigital.dto.StarwarsPlanetsPage;
import com.amedigital.entity.Planet;
import com.amedigital.repository.PlanetRepository;

import reactor.core.publisher.Flux;

@Component
public class StarWarsWebClient {

	private final String url = "https://swapi.dev/api";
	private final PlanetRepository planetRepository;
	private final WebClient webClient;

	public Flux<Planet> findByName(String name){

		Flux<Planet> a  =  webClient.get()
				.uri("/planets/?search="+name)
				.retrieve()
				.bodyToFlux(StarwarsPlanetsPage.class)
				.flatMap((StarwarsPlanetsPage page) -> {
					List<Planet> planets = page.getResults()
							.parallelStream()
							.map(p -> setTimeInFilms(p))
							.collect(Collectors.toList());

					return  planetRepository.saveAll(planets);
				});

		return a;
	}

	public void save(StarwarsPlanetsPage page) {
		List<Planet> planets = page.getResults()
				.parallelStream()
				.map(p -> setTimeInFilms(p))
				.collect(Collectors.toList());

		planetRepository.saveAll(planets).subscribe();	
	}

	public Planet setTimeInFilms(Planet p) {
		p.setTimesInFilms(p.getFilms().size());
		return p;
	}

	public StarWarsWebClient(PlanetRepository planetRepository, WebClient.Builder builder) {
		super();
		this.planetRepository = planetRepository;
		this.webClient = builder.baseUrl(url).build();
	}

}
