package com.amedigital.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amedigital.entity.Planet;
import com.amedigital.service.PlanetService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/planet")
public class PlanetController {
	
	private final PlanetService planetService;
	
	@GetMapping
	public Flux<Planet> findAll(){
		return planetService.findAll();
	}
	
	@GetMapping("/{id}")
	public Mono<Planet> findById(@PathVariable String id){
		return planetService.findById(id);
	}
	
	@GetMapping("/name/{name}")
	public Flux<Planet> findByName(@PathVariable String name){
		return planetService.findByName(name);
	}
	
	@PostMapping
	public Mono<Planet> save (@RequestBody Planet planet){
		return planetService.save(planet);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Mono<Void>> deleteById(@PathVariable String id){
		return ResponseEntity.status(HttpStatus.NO_CONTENT).body(planetService.delete(id));
	}

	public PlanetController(PlanetService planetService) {
		super();
		this.planetService = planetService;
	};
	
	

}
