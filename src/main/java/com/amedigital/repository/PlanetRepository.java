package com.amedigital.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.amedigital.entity.Planet;

import reactor.core.publisher.Flux;

@Repository
public interface PlanetRepository extends ReactiveMongoRepository<Planet, String> {
	Flux<Planet> findByNameContainingIgnoreCase(String name);
	Optional<Planet> findByNameIgnoreCase(String name);
}
