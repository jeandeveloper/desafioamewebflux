package com.amedigital.validator;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.amedigital.entity.Planet;

@Component
public class PlanetValidator {

	
	public static void validarPlanet(Planet planet) {
		if (Objects.isNull(planet.getName()) || planet.getName().isEmpty() || planet.getName().isBlank()) {
			throw new IllegalArgumentException("Campo name obrigatorio");			
		}
		if (Objects.isNull(planet.getClimate()) || planet.getClimate().isEmpty() || planet.getClimate().isBlank()) {
			throw new IllegalArgumentException("Campo climate obrigatorio ");			
		}
		if (Objects.isNull(planet.getTerrain()) || planet.getTerrain().isEmpty() || planet.getTerrain().isBlank()) {
			throw new IllegalArgumentException("Campo terrain obrigatorio ");			
		}	
	}
}
