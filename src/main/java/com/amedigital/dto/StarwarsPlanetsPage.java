package com.amedigital.dto;

import java.io.Serializable;
import java.util.List;

import com.amedigital.entity.Planet;

public class StarwarsPlanetsPage implements Serializable {
	
	private static final long serialVersionUID = 3980080414320272309L;
	private List<Planet> results;

	public List<Planet> getResults() {
		return results;
	}

	public void setResults(List<Planet> results) {
		this.results = results;
	}
}
