package com.amedigital.exception;

import java.util.NoSuchElementException;

import org.jboss.logging.Logger;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvisor{


	private static Logger logger = Logger.getLogger(ControllerAdvisor.class);
	  
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<String> serverExceptionHandler(IllegalArgumentException ex) {
		logger.error(ex.getMessage());
	    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
	}

	@ExceptionHandler(DuplicateKeyException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ResponseEntity<String> serverExceptionHandler(DuplicateKeyException ex) {
		logger.error(ex.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT).body("Registro já existe.");
	}
	
	@ExceptionHandler(NoSuchElementException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<String> serverExceptionHandler(NoSuchElementException ex) {
		logger.error(ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Registro não existe.");
	}
	
}
