package com.amedigital.service;

import java.util.NoSuchElementException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amedigital.entity.Planet;

import com.amedigital.repository.PlanetRepository;
import com.amedigital.validator.PlanetValidator;
import com.amedigital.webclient.StarWarsWebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class PlanetService {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarWarsWebClient starWarsWebClient;

	public Flux<Planet> findAll(){
		return planetRepository.findAll();
	}

	public Mono<Planet> findById(String id){
		return planetRepository.findById(id)
			    .switchIfEmpty(Mono.error(new NoSuchElementException()));
	}

	public Mono<Planet> save(Planet planet){
		PlanetValidator.validarPlanet(planet);
		return planetRepository.save(planet);
	}

	public Mono<Void> delete(String id){
		return planetRepository.deleteById(id);
	}

	@Transactional
	public Flux<Planet> findByName(String name){

		Flux<Planet> planets = planetRepository
				.findByNameContainingIgnoreCase(name)
				.switchIfEmpty(Flux.defer(() -> starWarsWebClient.findByName(name)));

		return planets;
	}

	public PlanetService(PlanetRepository planetRepository, StarWarsWebClient starWarsWebClient) {
		super();
		this.planetRepository = planetRepository;
		this.starWarsWebClient = starWarsWebClient;
	}

}
