package com.amedigital.entity;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Planet {
	
	@Id
	private String id;
	@Indexed(unique = true)
	private String name;
	private String climate;
	private String terrain;
	private List<String> films;
	private int timesInFilms;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getClimate() {
		return climate;
	}
	public void setClimate(String climate) {
		this.climate = climate;
	}
	public String getTerrain() {
		return terrain;
	}
	public void setTerrain(String terrain) {
		this.terrain = terrain;
	}
	public List<String> getFilms() {
		return films;
	}
	public void setFilms(List<String> films) {
		this.films = films;
	}
	public int getTimesInFilms() {
		return timesInFilms;
	}
	public void setTimesInFilms(int timesInFilms) {
		this.timesInFilms = timesInFilms;
	}

}
